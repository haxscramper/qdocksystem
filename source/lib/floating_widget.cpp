#include "floating_widget.hpp"

#include <QBoxLayout>
#include <QMouseEvent>
#include <QPushButton>
#include <QSizePolicy>
#include <QStyle>

#include "dock_manager.hpp"
#include "tab_title.hpp"
#include "tab_widget.hpp"
#include "floating_window.hpp"

namespace ads {

FloatingWidget::FloatingWidget(DockManager* _manager, std::unique_ptr<Tab> _tab)
    : QWidget(nullptr, Qt::CustomizeWindowHint | Qt::Tool)
    , titleLayout(new QBoxLayout(QBoxLayout::LeftToRight))
    , manager(_manager)
    , tab(std::move(_tab)) {
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);
    window             = new FloatingWindow();
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    setLayout(layout);

    // Title + Controls
    titleLayout->addWidget(tab->getTitleWidget(), 1);
    layout->addLayout(titleLayout, 0);

    QPushButton* closeButton = new QPushButton();
    closeButton->setObjectName("closeButton");
    closeButton->setFlat(true);
    closeButton->setIcon(
        style()->standardIcon(QStyle::SP_TitleBarCloseButton));
    closeButton->setToolTip(tr("Close"));
    closeButton->setSizePolicy(
        QSizePolicy::Expanding, QSizePolicy::Expanding);
    titleLayout->addWidget(closeButton);

    // Content
    layout->addWidget(tab->getTabWidget(), 1);
    tab->getTabWidget()->show();
    ADS_LOG << "Created floating widget";

    tab->setFloating(this);
}

FloatingWidget::~FloatingWidget() {
    ADS_LOG << "Destroyed floating widget";
    DockManager::closeEmptyManagers();
#ifdef ADS_DEEP_DEBUG
    DockManager::debugAll();
#endif
}


/*!
 * \brief Release floating widget from managet and move it to floating
 * window's [manager](\ref ads::FloatingWindow::manager)
 */
void FloatingWidget::setFreeFloating(QPoint gpos) {
    ADS_LOG << "Widget has been dropped";
    window->acceptFloating(manager->releaseFloating(), gpos);
}


/*!
 * \brief Remove tab content from all layouts, reset tab's title
 * [floating](\ref ads::TabTitle::floating) to null and return tab
 */
std::unique_ptr<Tab> FloatingWidget::releaseContent() {
    titleLayout->removeWidget(tab->getTitleWidget());
    layout()->removeWidget(tab->getTabWidget());
    tab->setFloating(nullptr);
    return std::move(tab);
}

/*!
 * \brief Get pointer to tab currently being managed by floating widget
 */
Tab* FloatingWidget::getTab() const {
    return tab.get();
}

DockManager* FloatingWidget::getManager() const {
    return manager;
}


} // namespace ads
