#include "tab.hpp"

#include <QLabel>
#include <QWidget>

#include "dock_manager.hpp"
#include "tab_title.hpp"
#include "tab_widget.hpp"
#include <support/misc.hpp>

namespace ads {

//  //////////////////////////////////////////////////////////////////////
//  Special member funcitons
//  //////////////////////////////////////////////////////////////////////

Tab::Tab(std::unique_ptr<QWidget> _widget, QString _title)
    : titleWidget(std::make_unique<TabTitle>(
          std::make_unique<QLabel>(_title),
          this))
    , contentWidget(new TabWidget(std::move(_widget), this)) {
    title = _title;
}

Tab::Tab(std::unique_ptr<QWidget> _widget, std::unique_ptr<QWidget> _title)
    : titleWidget(std::make_unique<TabTitle>(std::move(_title), this))
    , contentWidget(new TabWidget(std::move(_widget), this)) {
}


//  //////////////////////////////////////////////////////////////////////
//  Getters and setters
//  //////////////////////////////////////////////////////////////////////

FloatingWidget* Tab::getFloating() const {
    return titleWidget->getFloating();
}

void Tab::setFloating(FloatingWidget* value) {
    titleWidget->setFloating(value);
}


TabTitle* Tab::getTitleWidget() const {
    return titleWidget.get();
}

TabWidget* Tab::getTabWidget() const {
    return contentWidget.get();
}


/// \brief Return title of the widget
QString Tab::getTitle() const {
    return title;
}

void Tab::setTitle(const QString& _title) {
    title = _title;
}

DockWidget* Tab::getDock() const {
    return dock;
}

void Tab::setDock(DockWidget* value) {
    dock = value;
}


} // namespace ads
