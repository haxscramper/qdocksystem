#include "tab_title.hpp"

#include "dock_widget.hpp"

namespace ads {
void TabTitle::moveTab(QMouseEvent* ev) {
    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QPoint moveToPos = mapToParent(ev->pos()) - dragStartPos;
    moveToPos.setY(0);
    move(moveToPos);
}

bool TabTitle::tabTearOff(QMouseEvent* ev) {
    DockWidget* dock = findParentDockWidget(this);
    return !floating                           //
           && !dragStartPos.isNull()           //
           && (ev->buttons() & Qt::LeftButton) //
           && dock != nullptr
           && !dock->headerAreaGeometry().contains(
                  dock->mapFromGlobal(ev->globalPos()));
}


bool TabTitle::startMovingTab(QMouseEvent* ev) {
    DockWidget* section = findParentDockWidget(this);
    if (section == nullptr) {
        return false;
    }

    bool dragThresholdPassed = (ev->pos() - dragStartPos).manhattanLength()
                               >= dragStartThreshold;

    return !dragStartPos.isNull()              //
           && (ev->buttons() & Qt::LeftButton) //
           && dragThresholdPassed              //
           && section->headerAreaGeometry().contains(
                  section->mapFromGlobal(ev->globalPos()));
}


bool TabTitle::continueTabMove() {
    DockWidget* section = findParentDockWidget(this);
    return tabMoving && (section != nullptr);
}

bool TabTitle::movedTab() {
    return tabMoving && (findParentDockWidget(this) != nullptr);
}

} // namespace ads
