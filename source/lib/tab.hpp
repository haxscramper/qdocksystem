#pragma once

#include "tab_title.hpp"
#include "tab_widget.hpp"
#include <QPointer>
#include <QSharedPointer>
#include <QWeakPointer>
#include <memory>

class QWidget;

#include "support.hpp"

namespace ads {
class DockManager;
class TabTitle;
class TabWidget;
class FloatingWidget;
} // namespace ads


namespace ads {
/*!
 * \brief Wrapper class that manages TabWidget and TabTitle as one piece
 */
class Tab
{
  public:
    Tab(std::unique_ptr<QWidget> _widget, QString _title);
    Tab(std::unique_ptr<QWidget> _widget, std::unique_ptr<QWidget> _title);

  public:
    QString         uniqueName() const;
    TabTitle*       getTitleWidget() const;
    TabWidget*      getTabWidget() const;
    FloatingWidget* getFloating() const;
    void            setFloating(FloatingWidget* value);
    QString         getTitle() const;
    void            setTitle(const QString& _title);
    DockWidget*     getDock() const;
    void            setDock(DockWidget* value);

  private:
    DockWidget* dock = nullptr;
    /// Widget to use as title.
    std::unique_ptr<TabTitle> titleWidget;
    /// Content widget. Is an owner
    std::unique_ptr<TabWidget> contentWidget;

    QString title;
};

} // namespace ads
