TEMPLATE = lib
CONFIG += staticlib debug
TARGET = qdocksystem
DESTDIR = $$PWD/../main

QT += core gui widgets xml


MOC_DIR = build_moc
OBJECTS_DIR = build_obj
CONFIG         += object_parallel_to_source

CONFIG         += depend_includepath
CONFIG         += console
QMAKE_CXXFLAGS += -std=c++17


EXTERNAL_DIR = $$PWD/../../../

include($$EXTERNAL_DIR/DebugMacro/debug.pri)
include($$EXTERNAL_DIR/Support/support.pri)
include($$EXTERNAL_DIR/Algorithm/algorithm.pri)
include($$PWD/qdocksystem.pri)
