#include "tab_widget.hpp"

#include <QBoxLayout>

namespace ads {

TabWidget::TabWidget(std::unique_ptr<QWidget> _widget, Tab* _tab)
    : tab(_tab) {
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(_widget.get());
    setLayout(layout);
    widget = _widget.release();
    widget->setParent(this);
}

QWidget* TabWidget::getWidget() const {
    return widget;
}

} // namespace ads
