#ifndef SECTION_WIDGET_H
#define SECTION_WIDGET_H

#include <QDebug>
#include <QFrame>
#include <QHBoxLayout>
#include <QList>
#include <QPushButton>
#include <QScrollArea>
#include <QStackedLayout>
#include <QVBoxLayout>

#include "support.hpp"
#include "tab.hpp"
#include <halgorithm/qwidget_cptr.hpp>
#include <memory>


namespace ads {
class DockManager;
class TabTitle;
} // namespace ads

namespace ads {
/*!
 * \brief Manages multiple instances of TabContent.
 * Displays a title widget, which is clickable and will switch to
 * the contents associated to the title when clicked.
 */

class DockWidget : public QFrame
{
    Q_OBJECT
    friend class DockManager;


  public:
    explicit DockWidget(DockManager* parent);
    virtual ~DockWidget();


    QRect headerAreaGeometry() const;
    QRect contentAreaGeometry() const;

    void                 addTab(std::unique_ptr<Tab> tab);
    int                  indexOfTab(Tab* const tab) const;
    std::unique_ptr<Tab> releaseTab(Tab* _tab);
    int indexOfTabByPos(const QPoint& pos, QWidget* exclude = nullptr)
        const;

    int  currentIndex() const;
    void moveContent(int from, int to);
    bool isValidIndex(int index) const;
    int  tabCount() const;


    const std::vector<Tab*> getContents() const;
    DockManager*            getManager() const;
    void                    setManager(DockManager* value);


  protected:
    void showEvent(QShowEvent*) override;

  public slots:
    void setCurrentIndex(int index);

  private slots:
    void onTabTitleClicked();
    void onCloseButtonClicked();
    void onTabsMenuActionTriggered(bool);
    void updateTabsMenu();

  private:
    std::vector<TabTitle*> getTabTitles() const;
    TabTitle*              getTitle(int index) const;

    DockManager*                      manager; ///< Parent dock manager
    std::vector<std::unique_ptr<Tab>> tabs;    ///< List of all tab widgets


    /// Layout that contains tab scroll area, tab selector button and tab
    /// close button
    spt::qwidget_cptr<QHBoxLayout> headerLayout;
    spt::qwidget_cptr<QScrollArea> tabsScrollArea;  ///< Scroll area for
                                                    ///< tabs
    spt::qwidget_cptr<QWidget> tabsContainerWidget; ///< Widget with all
                                                    ///< tabs
    spt::qwidget_cptr<QHBoxLayout> tabsLayout; ///< Layout that contains
                                               ///< tabs
    spt::qwidget_cptr<QPushButton> tabsMenuButton;   ///< Dropdow button to
                                                     ///< show all tabs
    spt::qwidget_cptr<QPushButton>    closeButton;   ///< Close button
    spt::qwidget_cptr<QStackedLayout> contentLayout; ///< Layout with all
                                                     ///< docked widget
    /// Used for calculations on _tabsLayout modification calls. \todo Find
    /// out or remove. Possibly \deprecated
    int tabsLayoutInitCount;


    QPoint    _mousePressPoint;
    Tab*      _mousePressContent;
    TabTitle* _mousePressTitleWidget;
};

/// \brief Custom scrollable implementation for tabs
class TabsScrollArea : public QScrollArea
{
  public:
    TabsScrollArea(QWidget* parent = nullptr);

  protected:
    virtual void wheelEvent(QWheelEvent*);
};

} // namespace ads
#endif
