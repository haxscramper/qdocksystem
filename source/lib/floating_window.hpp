#ifndef FLOATINGWINDOW_HPP
#define FLOATINGWINDOW_HPP

#include <QMainWindow>
#include <memory>

namespace ads {
class DockManager;
class FloatingWidget;
} // namespace ads

namespace ads {
/*!
 * \brief Floating window that contains mutiple dock widgets.
 */
class FloatingWindow : public QMainWindow
{
    Q_OBJECT
  public:
    explicit FloatingWindow(QWidget* parent = nullptr);

    void acceptFloating(std::unique_ptr<FloatingWidget> _floating, QPoint gpos);

  private:
    DockManager* manager;
    QRect        targetGeometry;
    QPoint       targetPos;
    int          moveAttempts = 0;

    // QWidget interface
  protected:
    void moveEvent(QMoveEvent* event) override;
};


} // namespace ads

#endif // FLOATINGWINDOW_HPP
