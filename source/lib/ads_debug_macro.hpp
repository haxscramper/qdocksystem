#ifndef ADS_DEBUG_MACRO_HPP
#define ADS_DEBUG_MACRO_HPP

#include <hdebugmacro/all.hpp>

#define ADSWIDGET_DEBUG

#ifdef ADSWIDGET_DEBUG
#    define ADS_LOG LOG
#    define ADS_INFO INFO
#    define ADS_INFO_1 INFO_1
#    define ADS_INFO_2 INFO_2
#    define ADS_WARN WARN
#    define ADS_ERROR ERROR
#    define ADS_ONCE(message) LOG_ONCE(message)
#    define ADS_FUNC_BEGIN DEBUG_FUNCTION_BEGIN
#    define ADS_FUNC_END DEBUG_FUNCTION_END
#    define ADS_FUNC ADS_FUNC_BEGIN ADS_FUNC_END
#    define ADS_FUNC_RET(message) DEBUG_FUNCTION_PRE_RETURN(message)
/// \brief Ultra-detailed debug for each  dock, redock and widget
/// create-delete operations
#    define ADS_DEEP_DEBUG
#else
#    define ADS_LOG VOID_LOG
#    define ADS_FUNC_BEGIN
#    define ADS_FUNC_END
#    define ADS_FUNC
#    define ADS_ONCE(message)
#    define ADS_FUNC_RET(message)
#    define ADS_ALIGNED(__message, __value, __string_length)
#    define ADS_BOOL(__message, __expression)
#endif


#endif // ADS_DEBUG_MACRO_HPP
