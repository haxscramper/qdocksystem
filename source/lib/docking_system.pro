SOURCES += \
    $$PWD/support.cpp \
    $$PWD/dock_manager.cpp \
    $$PWD/dock_widget.cpp \
    $$PWD/tab.cpp \
    $$PWD/tab_title.cpp \
    $$PWD/tab_widget.cpp \
    $$PWD/drop_overlay.cpp \
    $$PWD/floating_widget.cpp \
    $$PWD/floating_window.cpp \
    $$PWD/tab_title_floating.cpp \
    $$PWD/tab_title_tab_move.cpp

HEADERS += \
    $$PWD/ads_debug_macro.hpp \
    $$PWD/tab.hpp \
    $$PWD/tab_title.hpp \
    $$PWD/tab_widget.hpp \
    $$PWD/floating_widget.hpp \
    $$PWD/floating_window.hpp \
    $$PWD/support.hpp \
    $$PWD/dock_manager.hpp \
    $$PWD/dock_widget.hpp \
    $$PWD/drop_overlay.hpp \
    $$PWD/ads_include.hpp

DISTFILES += \
    $$PWD/dev_notes.md

include($$PWD/../include/qdocking_system/include.pri)
