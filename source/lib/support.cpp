#include "support.hpp"

#include <QLayout>
#include <QSplitter>
#include <QVariant>
#include <QWidget>

#include "dock_manager.hpp"
#include "dock_widget.hpp"

namespace ads {
bool splitterContainsDock(QSplitter* splitter) {
    for (int i = 0; i < splitter->count(); ++i) {
        QWidget*    w        = splitter->widget(i);
        QSplitter*  splitter = qobject_cast<QSplitter*>(w);
        DockWidget* dock     = nullptr;
        if (splitter != nullptr && splitterContainsDock(splitter)) {
            return true;
        } else if ((dock = qobject_cast<DockWidget*>(w)) != nullptr) {
            return true;
        }
    }
    return false;
}

void deleteEmptySplitter(DockManager* container) {
    ADS_FUNC_BEGIN

    bool repeat = false;
    do {

        repeat                      = false;
        QList<QSplitter*> splitters = container
                                          ->findChildren<QSplitter*>();
        ADS_LOG << "Splitter count" <<  splitters.count();
        for (QSplitter* splitter : splitters) {
            bool to_delete = splitter->property("ads-splitter").toBool()
                             && splitter->count() == 0
                             && !splitterContainsDock(splitter);
            if (to_delete) {
                splitter->deleteLater();
            }
            ADS_LOG << "Repeat again. Breaking cycle";
            repeat = true;
            break;
        }
        LOG << "Splitter count" << splitters.count();
    } while (repeat);

    ADS_FUNC_END
}


DockManager* findParentContainerWidget(QWidget* w) {
    DockManager* cw   = nullptr;
    QWidget*     next = w;
    do {
        if ((cw = dynamic_cast<DockManager*>(next)) != nullptr) {
            break;
        }
        next = next->parentWidget();
    } while (next);
    return cw;
}

DockWidget* findParentDockWidget(class QWidget* w) {
    DockWidget* cw   = nullptr;
    QWidget*    next = w;
    do {
        if ((cw = dynamic_cast<DockWidget*>(next)) != nullptr) {
            break;
        }
        next = next->parentWidget();
    } while (next);
    return cw;
}

QSplitter* findParentSplitter(class QWidget* w) {
    QSplitter* splitter = nullptr;
    QWidget*   next     = w;
    do {
        if ((splitter = dynamic_cast<QSplitter*>(next)) != nullptr) {
            break;
        }
        next = next->parentWidget();
    } while (next);
    return splitter;
}

QSplitter* findImmediateSplitter(class QWidget* w) {
    QSplitter* splitter = nullptr;
    QLayout*   layout   = w->layout();
    if (!layout || layout->count() <= 0)
        return splitter;
    for (int i = 0; i < layout->count(); ++i) {
        QLayoutItem* li = layout->itemAt(0);
        if (!li->widget())
            continue;
        if ((splitter = dynamic_cast<QSplitter*>(li->widget())) != nullptr)
            break;
    }
    return splitter;
}

} // namespace ads
