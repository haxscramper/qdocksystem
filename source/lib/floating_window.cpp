#include "floating_window.hpp"

#include "dock_manager.hpp"
#include "floating_widget.hpp"
#include "tab.hpp"
#include "tab_title.hpp"
#include "tab_widget.hpp"

namespace ads {


//  //////////////////////////////////////////////////////////////////////
//  Special member funcitons
//  //////////////////////////////////////////////////////////////////////

FloatingWindow::FloatingWindow(QWidget* parent)
    : QMainWindow(parent), manager(new DockManager(this)) {
    setCentralWidget(manager);
    hide();
    setContentsMargins(0, 0, 0, 0);
    manager->setContentsMargins(0, 0, 0, 0);
}


//  //////////////////////////////////////////////////////////////////////
//  Public functions
//  //////////////////////////////////////////////////////////////////////

/// \brief Move content from floating widget into dock manager for this
/// window
void FloatingWindow::acceptFloating(
    std::unique_ptr<FloatingWidget> _floating,
    QPoint                          gpos) {
    ADS_FUNC_BEGIN

    show();
    resize(_floating->size());
    ADS_LOG << "Size" << _floating->size();

    manager->addTab(_floating->releaseContent());
    _floating->deleteLater();


    targetPos      = gpos - QPoint(0, 20);
    targetGeometry = _floating->geometry();

    ADS_LOG << "Target pos      " << targetPos;
    ADS_LOG << "Global pos      " << gpos;
    ADS_LOG << "Target geometry " << targetGeometry;

    moveAttempts = 0;

#ifdef ADS_DEEP_DEBUG
    DockManager::debugAll();
#endif

    ADS_FUNC_END
}

//  //////////////////////////////////////////////////////////////////////
//  QWidget interface
//  //////////////////////////////////////////////////////////////////////

void FloatingWindow::moveEvent(QMoveEvent* event) {
    ADS_LOG << "Moving floating window";
    if (moveAttempts < 5) {
        /// \todo Haven't found why but call to move() in
        /// FloatingWindow::acceptFloatingWidget almost never resulted in
        /// correct positioning of this window. This is temporary
        /// workaround that can result in \bug capable if infinite
        /// recursion. Contains workaround. Have no idea what causes
        /// problem or how to fix it.
        ADS_INFO << "Negating call to move";
        moveAttempts++;
        setGeometry(targetGeometry);
        move(targetPos);
    } else {
        QWidget::moveEvent(event);
    }
}

} // namespace ads
