#include "tab_title.hpp"

#include "dock_manager.hpp"
#include "dock_widget.hpp"
#include "drop_overlay.hpp"
#include "floating_widget.hpp"
#include "tab_widget.hpp"


namespace ads {
bool TabTitle::movingFloating(QMouseEvent* ev) {
    return floating != nullptr && (ev->buttons() & Qt::LeftButton);
}

void TabTitle::moveFloatingWidget(QMouseEvent* ev) {
    const QPoint targetPos = ev->globalPos()
                             - (dragStartPos
                                + QPoint(
                                      ADS_WINDOW_FRAME_BORDER_WIDTH,
                                      ADS_WINDOW_FRAME_BORDER_WIDTH));
    try {
        floating->move(targetPos);
    } catch (...) {
        DEBUG_WARN_IF_CALLED("floating->move(targetPos) exception")
        DockManager::debugAll();
    }

    DockManager* manager = DockManager::findUnderMouse();
    if (manager == nullptr) {
        DockManager::hideAllOverlays();
        return;
    }

    QPoint       localCursorPos = manager->mapFromGlobal(QCursor::pos());
    DropOverlay* overlay        = manager->getDropOverlay();
    DockWidget*  dock           = manager->dockAt(localCursorPos);

    if (manager->outerTopDropRect().contains(localCursorPos)) {
        overlay->setAllowedAreas(ads::TopDropArea);
        overlay->showDropOverlay(manager, manager->outerTopDropRect());
    } else if (manager->outerRightDropRect().contains(localCursorPos)) {
        overlay->setAllowedAreas(ads::RightDropArea);
        overlay->showDropOverlay(manager, manager->outerRightDropRect());
    } else if (manager->outerBottomDropRect().contains(localCursorPos)) {
        overlay->setAllowedAreas(ads::BottomDropArea);
        overlay->showDropOverlay(manager, manager->outerBottomDropRect());
    } else if (manager->outerLeftDropRect().contains(localCursorPos)) {
        overlay->setAllowedAreas(ads::LeftDropArea);
        overlay->showDropOverlay(manager, manager->outerLeftDropRect());
    } else if (dock != nullptr) {
        overlay->setAllowedAreas(ads::AllAreas);
        overlay->showDropOverlay(dock);
    } else {
        DockManager::hideAllOverlays();
    }
}

void TabTitle::beginFloatingWidget(QMouseEvent* ev) {
    ADS_FUNC_BEGIN

#ifdef ADS_DEEP_DEBUG
    DockManager::debugAll();
#endif

    DockWidget*  dock    = tab->getDock();
    DockManager* manager = dock->getManager();

    LOG << "Tearing off one tab. Dock tab count" << dock->tabCount();
    std::unique_ptr<FloatingWidget>
        newFloating = std::make_unique<FloatingWidget>(
            manager, dock->releaseTab(tab));
    LOG << "Removed tab from dock. New tab count" << dock->tabCount();
    ADS_INFO << "Created new floating widget";
    newFloating->resize(dock->size());

    const QPoint targetPos = ev->globalPos()
                             - (dragStartPos
                                + QPoint(
                                      ADS_WINDOW_FRAME_BORDER_WIDTH,
                                      ADS_WINDOW_FRAME_BORDER_WIDTH));
    newFloating->move(targetPos);
    newFloating->show();
    floating = newFloating.get();
    ADS_INFO << "Set TabTitle::floating to new one";

    manager->setFloating(std::move(newFloating));

    ADS_LOG << "Created floating widget. Cleaning up DockManager";
    manager->deleteEmpty();

    ADS_FUNC_END
}

bool TabTitle::overSideArea(QMouseEvent* ev) {
    return areaUnderMouse(ev) != ads::InvalidDropArea;
}

DropArea TabTitle::areaUnderMouse(QMouseEvent* ev) {
    DockManager* manager = DockManager::findUnderMouse();

    if (manager == nullptr) {
        return ads::InvalidDropArea;
    }

    QPoint mouseLocal = manager->mapFromGlobal(ev->globalPos());

    DropArea dropArea = ads::InvalidDropArea;
    if (manager->outerTopDropRect().contains(mouseLocal)) {
        dropArea = ads::TopDropArea;
    } else if (manager->outerRightDropRect().contains(mouseLocal)) {
        dropArea = ads::RightDropArea;
    } else if (manager->outerBottomDropRect().contains(mouseLocal)) {
        dropArea = ads::BottomDropArea;
    } else if (manager->outerLeftDropRect().contains(mouseLocal)) {
        dropArea = ads::LeftDropArea;
    }

    return dropArea;
}

void TabTitle::dropOntoContainer(QMouseEvent* ev) {
    ADS_FUNC_BEGIN

    DockManager* manager       = DockManager::findUnderMouse();
    QPoint       mouseLocalPos = manager->mapFromGlobal(ev->globalPos());
    DockWidget*  dock          = manager->dockAt(mouseLocalPos);
    DropArea dropArea = manager->getDropOverlay()->showDropOverlay(dock);

    ADS_INFO << "Moving floating widget ";
    ADS_LOG << "from: " << floating->getManager()->ID;
    ADS_LOG << "to:   " << manager->ID;
    if (floating->getManager() == manager) {
        ADS_INFO_2 << "Rearranging tabs in manager";
    }


    manager->getDropOverlay()->setAllowedAreas(ads::AllAreas);
    if (dropArea != InvalidDropArea) {
        manager->dropFloating(
            floating->getManager()->releaseFloating(), dock, dropArea);
        floating = nullptr;
    } else {
        floating->setFreeFloating(ev->globalPos());
    }

    DockManager::hideAllOverlays();

    ADS_FUNC_END
}

void TabTitle::dropOntoSideArea(QMouseEvent* ev) {
    ADS_FUNC_BEGIN
    DockManager* manager  = DockManager::findUnderMouse();
    DropArea     dropArea = areaUnderMouse(ev);

    ADS_INFO << "Moving floating widget ";
    ADS_LOG << "from: " << floating->getManager()->ID;
    ADS_LOG << "to:   " << manager->ID;
    if (floating->getManager() == manager) {
        ADS_INFO_2 << "Rearranging tabs in manager";
    }


    if (dropArea != ads::InvalidDropArea) {
        manager->dropFloating(
            floating->getManager()->releaseFloating(), nullptr, dropArea);
        floating = nullptr;
    } else {
        floating->setFreeFloating(ev->globalPos());
    }

    DockManager::hideAllOverlays();

    ADS_FUNC_END
}
} // namespace ads
