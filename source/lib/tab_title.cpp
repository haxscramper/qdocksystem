#include "tab_title.hpp"

#include <QApplication>
#include <QBoxLayout>
#include <QCursor>
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QSplitter>
#include <QString>
#include <QStyle>
#include <algorithm/all.hpp>
#include <algorithm>

#include "dock_manager.hpp"
#include "dock_widget.hpp"
#include "drop_overlay.hpp"
#include "floating_widget.hpp"
#include "tab.hpp"
#include "tab_widget.hpp"

namespace ads {

//  //////////////////////////////////////////////////////////////////////
//  Special member funcitons
//  //////////////////////////////////////////////////////////////////////


Tab* TabTitle::getTab() const {
    return tab;
}

TabTitle::TabTitle(std::unique_ptr<QWidget> widget, Tab* _tab)
    : tab(_tab)
    , tabWidget(widget.release())
//  ,
{
    tabWidget->setParent(this);
    initUI();
}

TabTitle::~TabTitle() {
    layout()->removeWidget(tab->getTitleWidget());
}

//          Setup functions

void TabTitle::initUI() {
    QBoxLayout* l = new QBoxLayout(QBoxLayout::LeftToRight);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->addWidget(tabWidget);
    setLayout(l);

    dragStartThreshold = QApplication::startDragDistance();
}

//  //////////////////////////////////////////////////////////////////////
//  QWidget interface
//  //////////////////////////////////////////////////////////////////////

void TabTitle::mousePressEvent(QMouseEvent* ev) {
    if (ev->button() == Qt::LeftButton) {
        ev->accept();
        dragStartPos = ev->pos();
        return;
    }
    QWidget::mousePressEvent(ev);
}

void TabTitle::mouseReleaseEvent(QMouseEvent* ev) {
    DockWidget*  dock    = nullptr;
    DockManager* manager = DockManager::findUnderMouse();

    if (manager != nullptr) {
        dock = manager->dockAt(manager->mapFromGlobal(ev->globalPos()));
    }


    if (floating != nullptr) {
        if (dock != nullptr) {
            if (overSideArea(ev)) {
                dropOntoSideArea(ev);
            } else {
                dropOntoContainer(ev);
            }
        } else {
            floating->setFreeFloating(ev->globalPos());
        }
    } else if (movedTab()) {
        QPoint    pos       = dock->mapFromGlobal(ev->globalPos());
        const int fromIndex = dock->indexOfTab(tab);
        const int toIndex   = dock->indexOfTabByPos(pos, this);
        dock->moveContent(fromIndex, toIndex);
    }

    if (!dragStartPos.isNull()) {
        emit clicked();
    }

    // Reset
    dragStartPos = QPoint();
    tabMoving    = false;
    if (manager != nullptr) {
        manager->getDropOverlay()->hideDropOverlay();
    }
    QWidget::mouseReleaseEvent(ev);
}

void TabTitle::mouseMoveEvent(QMouseEvent* ev) {
    if (movingFloating(ev)) {
        ADS_ONCE("Moving floating widget")
        ev->accept();
        moveFloatingWidget(ev);
        return;
    } else if (tabTearOff(ev)) {
        ADS_ONCE("Tearing tab off")
        ev->accept();
        beginFloatingWidget(ev);
        return;
    } else if (continueTabMove()) {
        ADS_ONCE("Continue tab movement")
        ev->accept();
        moveTab(ev);
        return;
    } else if (startMovingTab(ev)) {
        ADS_ONCE("Starting tab movement")
        ev->accept();
        tabMoving = true;
        raise();
        return;
    }
    QWidget::mouseMoveEvent(ev);
}


//  //////////////////////////////////////////////////////////////////////
//  Member access functions
//  //////////////////////////////////////////////////////////////////////

FloatingWidget* TabTitle::getFloating() const {
    return floating;
}

void TabTitle::setFloating(FloatingWidget* value) {
    floating = value;
}

bool TabTitle::isActiveTab() const {
    return activeTab;
}

void TabTitle::setActiveTab(bool active) {
    if (active != activeTab) {
        activeTab = active;

        style()->unpolish(this);
        style()->polish(this);
        update();

        emit activeTabChanged();
    }
}

} // namespace ads
