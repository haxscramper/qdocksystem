#ifndef ADS_CONTAINERWIDGET_H
#define ADS_CONTAINERWIDGET_H

/*!
 * \file DockManager.h
 * \brief DockManager class declaration
 *
 * \todo Remove `hiddenSectionItems`
 */

#include <memory>
#include <vector>

#include <QFrame>
#include <QGridLayout>
#include <QList>

#include <halgorithm/qwidget_cptr.hpp>

#include "support.hpp"


namespace ads {
class DockWidget;
class DropOverlay;
class FloatingWidget;
class Tab;
} // namespace ads

namespace ads {
/*!
 * \brief ContainerWidget is the main container to provide the docking
 * functionality. It manages multiple sections with all possible areas.
 * \todo Add `addTab` version for inserting widgets directly
 */
class DockManager : public QFrame
{
    Q_OBJECT

    /// All dock managers that are now opened
    static std::vector<DockManager*> dockManagers;

  public:
    explicit DockManager(QWidget* parent);
    virtual ~DockManager();

    static void         hideAllOverlays();
    static DockManager* findUnderMouse();
    static void         closeEmptyManagers();
    void                deleteEmpty();
    bool                isEmpty() const;
    void                close();

    // Debug
    int         ID;
    static void debugAll();
    void        logState() const;

    DockWidget* addTab(
        std::unique_ptr<Tab> tab,
        DockWidget*          sw   = nullptr,
        DropArea             area = CenterDropArea);


    void setFloating(std::unique_ptr<FloatingWidget> _floating);
    void dropFloating(
        std::unique_ptr<FloatingWidget> _floating,
        DockWidget*                     dock,
        DropArea                        area);
    std::unique_ptr<FloatingWidget> releaseFloating();


    QRect outerTopDropRect() const;
    QRect outerRightDropRect() const;
    QRect outerBottomDropRect() const;
    QRect outerLeftDropRect() const;

    std::vector<Tab*> getTabs() const;
    DropOverlay*      getDropOverlay();
    void              setCloseable(bool value);
    DockWidget*       dockAt(const QPoint& localPos) const;

  private:
    DockWidget* findTabInDocks(Tab* const tabContent);

    DockWidget* newDockWidget();
    DockWidget* insertTab(
        std::unique_ptr<Tab> tab,
        DockWidget*          dock,
        DropArea             area);

    DockWidget* dropIntoOuterArea(std::unique_ptr<Tab> tab, DropArea area);
    DockWidget* dropIntoDock(
        std::unique_ptr<Tab> tab,
        DockWidget*          dock,
        DropArea             area);
    DockWidget* dropContentOuterHelper(
        std::unique_ptr<Tab> tab,
        Qt::Orientation      orientation,
        bool                 append);


  private slots:
    void onActiveTabChanged();

  signals:
    void orientationChanged();

    /*!
     * Emits whenever the "isActiveTab" state of a SectionContent changes.
     * Whenever the users sets another tab as active, this signal gets
     * invoked for the old tab and the new active tab (the order is
     * unspecified).
     */
    void activeTabChanged(const Tab* sc, bool active);

    /*!
     * Emits whenever the visibility of a SectionContent changes.
     * \see showSectionContent(), hideSectionContent()
     */
    void sectionContentVisibilityChanged(Tab* const sc, bool visible);

  private:
    bool closeable = true;
    /// Docks inside dock manager.
    std::vector<std::unique_ptr<DockWidget>> docks;
    std::unique_ptr<FloatingWidget>          floating;

    spt::qwidget_cptr<DropOverlay> dropOverlay;
    spt::qwidget_cptr<QGridLayout> mainLayout;
    Qt::Orientation                _orientation;
};

} // namespace ads
#endif
