#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <qdocksystem/dockmanager.hpp>

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    MainWindow(QWidget* parent = nullptr);
    ads::DockManager* manager;
};

#endif // MAINWINDOW_HPP
