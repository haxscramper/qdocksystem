TEMPLATE = app
TARGET =  demo
DESTDIR = $$PWD
CONFIG += debug

QT += core gui widgets

MOC_DIR     = build_moc
OBJECTS_DIR = build_obj

CONFIG         += object_parallel_to_source
CONFIG         += depend_includepath
CONFIG         += console

QMAKE_CXXFLAGS += -std=c++17

EXTERNAL_DIR = $$PWD/../../../

INCLUDEPATH *= \
    $$EXTERNAL_DIR/Algorithm/include \
    $$EXTERNAL_DIR/DebugMacro/include \
    $$EXTERNAL_DIR/Support/include \
    $$PWD/../../include

PRE_TARGETDEPS *= $$PWD/libqdocksystem.a
LIBS *= -L$$PWD/ -lqdocksystem


SOURCES *= $$PWD/*.cpp
HEADERS *= $$PWD/*.hpp
