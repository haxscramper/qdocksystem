#include "mainwindow.hpp"
#include <QTextEdit>
#include <debugmacro/all.hpp>
#include <memory>
#include <qdocksystem/dockwidget.hpp>
#include <qdocksystem/tab.hpp>

MainWindow::MainWindow(QWidget* parent) : QMainWindow(parent) {
    manager = new ads::DockManager(this);
    setCentralWidget(manager);
    manager->addTab(std::make_unique<ads::Tab>(
        std::make_unique<QTextEdit>(), "Test 1"));
    manager->addTab(std::make_unique<ads::Tab>(
        std::make_unique<QTextEdit>(), "Test 2"));
    manager->addTab(std::make_unique<ads::Tab>(
        std::make_unique<QTextEdit>(), "Test 3"));
}
