#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QBoxLayout>
#include <QCalendarWidget>
#include <QFileSystemModel>
#include <QFrame>
#include <QLabel>
#include <QTextEdit>
#include <QTime>
#include <QTreeView>
#include <ads_include.hpp>
#include <algorithm.hpp>
#include <support.hpp>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , dockManager(new ads::DockManager(this)) {
    ui->setupUi(this);

    setCentralWidget(dockManager);

    ads::DockWidget* dock = dockManager->addTab(
        spt::make_uptr(
            new ads::Tab(spt::make_uptr(new QCalendarWidget()), "Hello")),
        nullptr,
        ads::BottomDropArea);

    for (int i = 0; i < 3; ++i) {
        dockManager->addTab(
            spt::make_uptr(new ads::Tab(
                spt::make_uptr(new QCalendarWidget()), "Hello")),
            dock,
            ads::CenterDropArea);
    }

    resize(800, 600);
}

MainWindow::~MainWindow() {
    delete ui;
}
