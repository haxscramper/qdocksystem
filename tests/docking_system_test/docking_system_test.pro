TARGET = AdvancedDockingSystemDemo

include($$PWD/../../top_buildflags.pri)

SOURCES += \
        $$PWD/main.cpp \
        $$PWD/mainwindow.cpp

HEADERS += \
        $$PWD/mainwindow.h

FORMS += \
        $$PWD/mainwindow.ui

BUILD_DIR = $$shadowed($$PWD/..)

PRE_TARGETDEPS += $$BUILD_DIR/docking_system/libdocking_system.a
LIBS += -L$$BUILD_DIR/docking_system -ldocking_system

INCLUDEPATH += $$QIUCUS_PARENT_DIR/DebugMacro
INCLUDEPATH += $$QIUCUS_PARENT_DIR/Algorithm
INCLUDEPATH += $$QIUCUS_PARENT_DIR/Support
INCLUDEPATH += $$PWD/../docking_system
DEPENDPATH  += $$PWD/../docking_system
